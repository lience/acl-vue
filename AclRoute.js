import 'react';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router';
import {PrivateRoute} from 'mre-authentication-react/components/PrivateRoute';
import {connect} from "react-redux";
import * as AclActions from '../redux/actions';
import * as aclSelectors from '../redux/selectors';
import {bindActionCreators} from "redux";

class AclRoute extends PrivateRoute {
    componentWillMount() {
        super.componentWillMount();

        // Lógica para validar o acl dessa rota
        if (this.props.permissoes && this.props.acl && this.props.acl.length > 0) {
            if (!this.props.hasAccess) {
                // O usuário não tem permissão pra esta rota.
                console.error('LIENCE-ACL: O usuário não tem permissão pra esta rota.');
                this.props.history.push('/');

                return this.willRender = false;
            }
        }
    }
}

AclRoute.propTypes = {
    acl: PropTypes.arrayOf(PropTypes.string)
};

/**
 * Conecta o State do Redux nas Propriedades do Componente
 *
 * @param state State do Redux com os Reducers
 * @param ownProps
 * @return {{permissoes: *}} Objeto que será fundido às props do Componente
 */
function mapStateToProps(state, ownProps) {
    return {
        permissoes: aclSelectors.getPermissoes(state),
        hasAccess: aclSelectors.checkAcl(state, ownProps.acl),
    };
}

/**
 * Mapeia as Actions do ACL nas Propriedades do Componente
 *
 * @return {{aclActions: {}}} Objeto que será fundido às props do Componente
 */
function mapDispatchToProps(dispatch) {
    return {
        aclActions: bindActionCreators(AclActions, dispatch),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AclRoute));
