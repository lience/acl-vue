import * as types from './mutation-types'

export default {
    [types.SET_ACL_ROUTES] (state, action) {
        state.routes = action.routes
    },
    [types.LOAD_ACL_SUCCESS] (state, action) {
        state.permissoes = action.permissoes;
    },
    [types.LOAD_ACL_ERROR] (state, action) {
        state.permissoes = [];
        state.error = action.error;
    },
};