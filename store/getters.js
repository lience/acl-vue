export default {
    getRoutes: (state) => {
        return state.routes;
    },
    getPermissoes: (state) => {
        // Caso o Serviço dê erro, permissões será `false`
        return state.permissoes;
    },
    findMatch: (state) => (name) => {
        if (!state.routes || !name) {
            return;
        }
        return state.routes.find((route) => route.name === name);
    },
    checkAcl: (state, getters) => (name) => {
        if (!name) {
            return true;
        }

        let arrPermissoes = getters.getPermissoes;
        let route = getters.findMatch(name);
        if (!route) {
            return false;
        }
        let arrMultAcl = route.props.acl;
        if (Array.isArray(arrMultAcl)) {
            return getters.checkAclInArray({arrMultAcl, arrPermissoes})
        }
        return !!arrPermissoes.find(permissao => permissao.name === arrMultAcl);
    },
    checkAclInArray: () => (objParams) => {
        let arrMultAcl = objParams.arrMultAcl;
        let aclMatches = arrMultAcl.map(acl => {
            return objParams.arrPermissoes.find(permissao => permissao.name === acl)
        });
        return !!(aclMatches.length === arrMultAcl.length && aclMatches[0]);
    },
};