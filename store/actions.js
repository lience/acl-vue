import * as types from './mutation-types'
import {resources} from '@/plugins/config'
import axios from 'axios'

export default {
    setRoutes({commit}, routes) {
        commit(types.SET_ACL_ROUTES, {routes});
    },
    loadPermissoes({commit}) {
        const urlPermissoes = `${resources.api}user/role`;
        return axios.get(urlPermissoes).then(response => {
            commit(types.LOAD_ACL_SUCCESS, {permissoes: response.data});
        }).catch(error => {
            return commit(types.LOAD_ACL_ERROR,
                {
                    type: types.LOAD_ACL_ERROR,
                    error: error,
                    permissoes: []
                }
            );
        });
    }
};