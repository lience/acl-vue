export const LOAD_ACL_SUCCESS   = 'acl.LOAD_ACL_SUCCESS';
export const LOAD_ACL_ERROR     = 'acl.LOAD_ACL_ERROR';
export const SET_ACL_ROUTES     = 'acl.SET_ACL_ROUTES';